#include <iostream>
#include <string>
#include <cstdlib>
#include <functional>
#include "HashMap.h"

using std::cout;
using std::string;

void doTest(bool succ, string name) {
  string ret;
  if(succ){
    ret = "\033[1;32m"+name+" test succeeded\033[0m";
  } else {
    ret = "\033[1;31m"+name+" test failed\033[0m";
  }
  cout << ret << "\n";
}

void subTest(string message) {
  cout << "\033[1;32m    "+message+" subtest\033[0m\n";
}

template<typename T>
bool mapIntTest(T &map) {
  // write test
  map[1] = 1;
  if(map[1] != 1) return false;
  map[2] = 13456;
  if(map[2] != 13456) return false;
  subTest("basic insert");
  HashMap<int,int,std::function<int(int)>> mapClone([] (int i) { return i; });
  mapClone[1] = 1;
  mapClone[2] = 13456;
  if(mapClone!=map) return false;
  subTest("equality");
  if(map.size()!=2) return false;
  subTest("size");
  mapClone.clear();
  if(mapClone.size()!=0) return false;
  subTest("clear");
  if(map.count(2)!=1) return false;
  subTest("count");
  map.erase(2);
  if(map.count(2)!=0) return false;
  subTest("erase");
  return true;
}

template<typename T>
bool mapStringTest(T &map) {
  // write test
  map[1] = "1";
  if(map[1] != "1") return false;
  map[2] = "13456";
  if(map[2] != "13456") return false;
  subTest("basic insert");
  HashMap<int,string,std::function<int(int)>> mapClone([] (int i) { return i; });
  mapClone[1] = "1";
  mapClone[2] = "13456";
  if(mapClone!=map) return false;
  subTest("equality");
  if(map.size()!=2) return false;
  subTest("size");
  mapClone.clear();
  if(mapClone.size()!=0) return false;
  subTest("clear");
  if(map.count(2)!=1) return false;
  subTest("count");
  map.erase(2);
  if(map.count(2)!=0) return false;
  subTest("erase");
  return true;
}

template <typename T>
bool scaleInsert(T &lst) {
  lst.clear();
  for(int i = 0; i<2000; ++i) {
    lst.insert(make_pair(i,i));
  }
  for(int i = 0; i<2000; ++i) {
    if(lst[i]!=i) return false;
  }
  subTest("insert");
  // TODO: Check contains
  lst.clear();
  return true;
}

template <typename T>
bool scaleErase(T &lst) {
  lst.clear();
  for(int i = 0; i<2000; ++i) {
    lst.insert(make_pair(i,i));
  }
  for(int i = 0; i<2000; ++i) {
    if(lst[i]!=i) return false;
  }
  subTest("insert");
  if(lst.erase(20)!=1) return false;
  if(lst.size()!=1999) return false;
  for(int i = 0; i<2000; ++i) {
    if((lst[i]!=i) && i!=20) return false;
  }
  subTest("erase");
  lst.clear();
  return true;
}

int main() {
  HashMap<int,int,std::function<int(int)>> map([] (int i) { return i; });
  doTest(mapIntTest(map), "int map basic");
  HashMap<int,int,std::function<int(int)>> map2([] (int i) { return i; });
  doTest(scaleInsert(map2), "scale insert");
  doTest(scaleErase(map2), "scale erase");
  HashMap<int,string,std::function<int(int)>> mapStr([] (int i) { return i; });
  doTest(mapStringTest(mapStr),"string map basic");
  return 0;
}
