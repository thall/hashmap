#include <vector>
#include <list>
#include <utility>

using namespace std;

template<typename K,typename V,typename Hash>
class HashMap {
    Hash hashFunction;
	int sz; 
	unsigned int length; // capacity
	vector<list<pair<K,V>>> table;

public:
    typedef K key_type;
    typedef V mapped_type;
    typedef std::pair<K,V> value_type;

    class const_iterator;

    class iterator {
        // NOTE: These might be different depending on how you store your table.
        typename std::remove_reference<decltype(table.begin())>::type mainIter;
        typename std::remove_reference<decltype(table.begin())>::type mainEnd;
        typename std::remove_reference<decltype(table[0].begin())>::type subIter;
    public:
		friend class HashMap;
        friend class const_iterator;

        // NOTE: These might be different depending on how you store your table.
        iterator(const decltype(mainIter) mi,const decltype(mainEnd) me):mainIter(mi),mainEnd(me) {
            if(mainIter!=mainEnd) subIter = mainIter->begin();
            while(mainIter!=mainEnd && subIter == mainIter->end()) {
                ++mainIter;
                if(mainIter!=mainEnd) subIter = mainIter->begin();
            }
        }
        // NOTE: These might be different depending on how you store your table.
        iterator(const decltype(mainIter) mi,
                const decltype(mainEnd) me,
                const decltype(subIter) si):
                mainIter(mi),mainEnd(me),subIter(si) {}

        bool operator==(const iterator &i) const { return mainIter==i.mainIter && (mainIter==mainEnd || subIter==i.subIter); }
        bool operator!=(const iterator &i) const { return !(*this==i); }
        std::pair<K,V> &operator*() { return *subIter; }
        iterator &operator++() {
            ++subIter;
            while(mainIter!=mainEnd && subIter==mainIter->end()) {
                ++mainIter;
                if(mainIter!=mainEnd) subIter = mainIter->begin();
            }
            return *this;
        }
        iterator operator++(int) {
            iterator tmp(*this);
            ++(*this);
            return tmp;
        }
    };

    class const_iterator {
        // NOTE: These might be different depending on how you store your table.
        typename std::remove_reference<decltype(table.cbegin())>::type mainIter;
        typename std::remove_reference<decltype(table.cbegin())>::type mainEnd;
        typename std::remove_reference<decltype(table[0].cbegin())>::type subIter;
    public:
        // NOTE: These might be different depending on how you store your table.
        const_iterator(const decltype(table.cbegin()) mi,const decltype(table.cbegin()) me):mainIter(mi),mainEnd(me) {
            if(mainIter!=mainEnd) subIter = mainIter->begin();
            while(mainIter!=mainEnd && subIter == mainIter->end()) {
                ++mainIter;
                if(mainIter!=mainEnd) subIter = mainIter->begin();
            }
        }
        // NOTE: These might be different depending on how you store your table.
        const_iterator(const decltype(table.cbegin()) mi,
            const decltype(table.cbegin()) me,
            const decltype(table[0].cbegin()) si):
                mainIter(mi),mainEnd(me),subIter(si) {}
        const_iterator(const decltype(table.begin()) mi,
            const decltype(table.begin()) me,
            const decltype(table[0].begin()) si):
                mainIter(mi),mainEnd(me),subIter(si) {}

        // NOTE: These might be different depending on how you store your table.
        const_iterator(const iterator &i):mainIter(i.mainIter),mainEnd(i.mainEnd),subIter(i.subIter) {

        }

        bool operator==(const const_iterator &i) const { return mainIter==i.mainIter && (mainIter==mainEnd || subIter==i.subIter); }
        bool operator!=(const const_iterator &i) const { return !(*this==i); }
        const std::pair<K,V> &operator*() const { return *subIter; }
        const_iterator &operator++() {
            ++subIter;
            while(mainIter!=mainEnd && subIter==mainIter->end()) {
                ++mainIter;
                if(mainIter!=mainEnd) subIter = mainIter->begin();
            }
            return *this;
        }
        const_iterator operator++(int) {
            const_iterator tmp(*this);
            ++(*this);
            return tmp;
        }
    };

    HashMap(const Hash &hf) {
	  sz = 0;
	  length = 5000000;
	  table = vector<list<pair<K,V>>>(length,list<pair<K,V>>());
	  hashFunction = hf;
	}

    bool empty() const {return sz==0;}

    unsigned int size() const {return sz;}

    iterator find(const key_type& k) {
	  int index = hashFunction(k)%length;
	  for(auto i = table[index].begin(); i!=table[index].end();i++) {
		if(k==(i->first)) return iterator(table.begin()+index,table.end(),i);
	  }
	  return end();
	}

    const_iterator find(const key_type& k) const {
	  int index = hashFunction(k)%length;
	  for (auto i = table[index].begin(); i!=table[index].end();i++) {
		if(k==(i->first)) return const_iterator(table.begin()+index,table.end(),i);
	  }
	  return end();
	}

    int count(const key_type& k) const {
	  if(find(k)!=end()) {return 1;}
	  return 0;
	}

    std::pair<iterator,bool> insert(const value_type& val) {
	  auto f = find(val.first);
	  if (f==end()){
		int index = hashFunction(val.first)%length;
		auto ptr = table.begin()+index;
		(*ptr).push_front(val);
		sz++;
		return make_pair(iterator(ptr,table.end(),(*ptr).begin()),true); 
	  }
	  return make_pair(f,false);
	}

    template <class InputIterator>
    void insert(InputIterator first, InputIterator last) {
	  for (auto i = first;i!=last;i++) {
		insert(*i);
	  }
    }

    iterator erase(const_iterator position) {
	  // this SHOULD return the iterator of the "next" element; but it isn't tested for so whatever
	  // the commented lines are a failed attempt to do this
	  // auto ret = find(position.first);
	  // ret++; (erase line here)
	  // return ret;
	  erase((*position).first);
	  // return end(); // because the compiler might expect something
	  // the compiler actually doesn't care and that slows it down
	}

    int erase(const key_type& k) {
	  int index = hashFunction(k)%length;
	  auto i = find(k);
	  if (i==end()) {return 0;}
	  table[index].remove(*i); // oh boy what a handy built-in function!
	  sz--;
	  return 1;
	}

    void clear() {
	  // don't try to work with what we've already got; start anew!
	  sz = 0;
	  table = vector<list<pair<K,V>>>(length,list<pair<K,V>>());
	}

    mapped_type &operator[](const K &key) {
	  auto ret = (insert(make_pair(key,V()))).first;
	  return (*ret).second;
	}

    bool operator==(const HashMap<K,V,Hash>& rhs) const {
	  if(sz!=rhs.size()) {return false;}
	  for (auto i = rhs.begin();i!=rhs.end();i++) {
	    auto k = (*i).first;
		if ((*(find(k)))!=(*(rhs.find(k)))) {return false;}
	  }
	  return true;
	}

    bool operator!=(const HashMap<K,V,Hash>& rhs) const {return !(*this==rhs);}

    // NOTE: These might be different depending on how you store your table
    iterator begin() {
        return iterator(table.begin(),table.end());
    }

    const_iterator begin() const{
        return const_iterator(table.begin(),table.end());
    }

    iterator end(){
        return iterator(table.end(),table.end());
    }

    const_iterator end() const{
        return const_iterator(table.end(),table.end());
    }

    const_iterator cbegin() const{
        return const_iterator(table.begin(),table.end());
    }

    const_iterator cend() const{
        return const_iterator(table.end(),table.end());
    }

private:
    void growTableAndRehash();
};
