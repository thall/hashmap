#!/bin/bash
# g++ Main.cpp -std=c++11 -Wall --pedantic -O2 -o prog
: ${CXX:=g++}
: ${CFLAGS:= --std=c++11 --pedantic -Wall -g -o prog}
: ${FILES:="
        Main.cpp
    "}
$CXX $CFLAGS $FILES
