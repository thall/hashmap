#!/bin/bash
# scala /users/xjiang/ProgramSubmitTest.jar F 1 ArrayStack.h ArrayQueue.h -std=c++11 -O2
: ${SUBMIT:=scala /users/xjiang/ProgramSubmitTest.jar}
: ${SFLAGS:= F 1}
: ${CFLAGS:= -std=c++11 -march=native -mtune=native -Ofast -pipe}
: ${FILES:= "
		HashMap.h
  "}
$SUBMIT $SFLAGS $FILES $CFLAGS
